
public class HotelRoom {
	
	int rmNum;
	boolean smokingPref;
	

	public int getrmNum() {
		return this.rmNum;
	}
	public void setRoomNum(int num) {
		this.rmNum = num;
	}
	
	public boolean getSmokPref() {
		return this.smokingPref;
	}
	public void setSmokPref(boolean p) {
		this.smokingPref = p;
	}
}
