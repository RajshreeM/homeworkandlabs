
public class RentalCar {
private long id;
private String make;
private String model;


public String getMake() {
	return this.make;
}
public String getModel() {
	return this.model;
}
public long getId() {
	return this.id;
}

public void setMake(String m) {
	this.make =m;
}

public void setModel(String m) {
	this.model =m;
}

public void setId(long m) {
	this.id =m;
}

}
