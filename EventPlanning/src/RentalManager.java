
public class RentalManager {
	RentalCar[] availCar = new RentalCar[50];
	HotelRoom[] availRoom = new HotelRoom[50];
	
	
	//displays rental cats
	public void displayAllCars() {
		for(int i =0; i < availCar.length;i++) {
			System.out.println("Make: "+availCar[i].getMake()+" Model: "+ availCar[i].getModel()+ "ID: "+ availCar[i].getId());
		}
	}
	
	
	//displays hotel rooms
	
	public void displayHotelRooms(HotelRoom[] rooms) {
		for(int i =0; i < rooms.length;i++) {
			System.out.println("RoomNum: "+rooms[i].getrmNum()+" Smoking Preg: "+ rooms[i].getSmokPref());
		}
	}

}
