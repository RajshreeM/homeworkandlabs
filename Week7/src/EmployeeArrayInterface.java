
public interface EmployeeArrayInterface {
	
	public void createEmployee(Employee employee);
	
	abstract void resizeEmployees();
	
	public Employee findEmployee(int empId);
	public Employee[] findAll();
	public boolean deleteEmployee(int empId);
	public boolean updateEmployee(Employee employee);
	public double displayHRA(int empId);
	public Double calculateGrossSal(int empId);
	

}
