import java.io.Console;
import java.util.Arrays;

public class EmployeeArrayImp1 implements EmployeeArrayInterface {

    private Employee[] employees;
    private int size;

    public EmployeeArrayImp1() {
    	employees = new Employee[10];
    }
    
    
    
    public void createEmployee(Employee employee) {
    	
    	try {
        if (size == employees.length && employee.salary<5000) {
            resizeEmployees();
        }

        employees[size++] = employee;
    	}
    	catch(Exception e) {
    		System.out.println("Invalid Salary Exception");
    	}
    }

    public void resizeEmployees() {
        employees = Arrays.copyOf(employees, size * 2);
    }

    public Employee findEmployee(int empId) {
        for (int i = 0; i < size; i++) {
            if (employees[i].getId() == empId) {
                return employees[i];
            }
        }
        return null;
    }

    public Employee[] findAll() {
        return Arrays.copyOfRange(employees, 0, size);
    }

    public boolean deleteEmployee(int empId) {
    	
        boolean flag = false;
        try {
        	try {
        for (int i = 0; i < size; i++) {

            if (employees[i].getId() == empId) {
                for (int j = i; j < size-1; j++) {
                    employees[j] = employees[j + 1];                       // shift
                }
                flag = true;
                employees[size--] = null;
            }
        }
        return flag;
        	}catch(ArithmeticException e) {
        		System.out.println("Given if is invalid. Please provide valid employee id");
        	}
        }
        catch (ArrayIndexOutOfBoundsException e) {
        	System.out.println("Cannot delete employee-out of bounds");
        	}
        
        
    }

    public boolean updateEmployee(Employee employee) {
    	
    	try {
    	if(employee.salary>5000) {
        boolean flag = false;
        for (int i = 0; i < size; i++) {
            if (employees[i].getId() == employee.getId()) {
                employees[i] = employee;
                flag = true;
            }
        }
        return flag;
    	}
    	}
    	catch(Exception e) {
    		System.out.println("Cannot update employee--error--Invalid Salary Sa");
    	}
    }


    public double displayHRA(int empId) {
        double hra = 0;
        for (int i = 0; i < size; i++) {
            if (employees[i].getId() == empId) {
                hra = employees[i].calculateHRA();
                break;
            }
        }

        return hra;
    }

    public Double calculateGrossSal(int empId) {
        for (int i = 0; i < size; i++) {
            if (employees[i].getId() == empId) {
                return employees[i].calculateGrossSal();
            }
        }
        return null;
    }
}



}
