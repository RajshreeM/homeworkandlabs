
public class Square extends Rectangle{
	
	double side =0.0;
	
	
	
	
	public Square(int s) {
		
		super(s,s);
		this.side = s;
	}
	
	
	public static void main(String[] args) {
		printSquares();
		test();
		

		
		
		
		
		
	}
	
	
	public static void test() {
		//calls method name calling of  'Shape' and 'Rectangle' class by the object of 'Square' class.
		Square name = new Square(5);
		
		
		//calls method of Rectangle's name
		name.printName();
		
		
	}
	
	
	
	/*
	 * Print 10 squares
	 */

	public static void printSquares() {
		
		for(int i=1;i<11;i++) {
			Square s = new Square(i);
			
			System.out.print("Area for Square with side " + i + " is ");
			s.printArea();
			
			
			
			
		}
		
		
		
		
				
				
				
		
	}
	
	/*
	 * prints name
	 * 
	 */
	public void printName() {
		super.printName();
		System.out.println("A square is a rectangle");
	}
	
	
}
