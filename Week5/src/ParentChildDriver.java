
public class ParentChildDriver {

	public static void main(String[] args) {
		
		ParentChild par = new ParentChild();
		
		par.printClass();
		
		ChildClass ch = new ChildClass();
		
		ch.printClass();
		
		
		// method of parent class by object of child class

	   ch.parentPrint();
	   
		
		
		
	}
}
