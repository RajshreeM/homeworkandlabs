
public class DailyMessage {
	
	public String getMessage(String day) {
		String s = " ";
		switch(day) {
		case "Monday": case "Wednesday": case "Friday":
			s= "study day";
			break;
		case "Tuesday": case "Thursday":
			s= "gym day";
			break;
		case "Saturday": case "Sunday":
			s = "weekend";
			break;
			
		default: 
			s= "invalid day";
		}
		return s;
			
	}

}
