
public class DailyMessageWithEnums {
	
	public enum DayOfTheWeek{MONDAY,TUESDAY,WEDNESDAY,THURSDAY,FRIDAY,SATURDAY,SUNDAY;}
	
	
	public String getMessage(DayOfTheWeek day) {
		String s = " ";
		switch(day) {
		case MONDAY: case WEDNESDAY: case FRIDAY:
			s= "study day";
			break;
		case TUESDAY: case THURSDAY:
			s= "gym day";
			break;
		case SATURDAY: case SUNDAY:
			s = "weekend";
			break;
			
		default: 
			s= "invalid day";
		}
		return s;
			
	}

}
