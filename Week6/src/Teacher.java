
public class Teacher extends Person{
	
	private int numCourses;
	private String[] courses;
	
	
	
	public Teacher(String name, String address) {
		super(name,address);
		
	}
	
	public String toString() {
		return "Teacher num courses: "+ numCourses;
	}
	public boolean addCourse(String course) {
		
		for(int i=0;i<courses.length;i++) {
			if(courses[i]==null) {
				courses[i] = course;
				return true;
			}
		}
		return false;
	}
	
	
	public boolean removeCourse(String course) {
		boolean found = false;
		for(int i=0;i<courses.length;i++) {
			if(courses[i].equalsIgnoreCase(course)) {
				found = true;
				
				
			}
			
		
	     }
		
		return found;
	}
	

}
