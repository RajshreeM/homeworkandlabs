
public class Person {

	
	private String name = "Placeholder";
	private String address = "123 Long Lane Ave";
	
	
	
	
	public Person(String name, String address) {
		this.name = name;
		this.address = address;
	}
	
	
	public String getName() {
		return name;
	}
	
	public String getAddress() {
		return address;
	}
	
	
	
	public void setAddress(String addr) {
		this.address = addr;
	}
	
	public String toString() {
		return "Name: "+this.name+" has the address : "+ this.address;
	}
}


