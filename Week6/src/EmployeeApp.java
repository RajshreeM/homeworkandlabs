
public class EmployeeApp {

	
	
	public static void main(String[] args) {
		Employee2 emp = new Employee2(1,"Rajshree", 50000);
		System.out.println(emp.toString());
		
		System.out.println(emp.HRA());
		
		Employee2 emp2 = new Employee2(1,"John", 51000);
		
		Employee2 emp3 = new Employee2(1,"Lucy", 52000);
		Employee2 emp4 = new Employee2(1,"Mary", 53000);
		Employee2 emp5 = new Employee2(1,"David", 54000);
		Employee2 emp6 = new Employee2(1,"Mark", 55000);
		Employee2 emp7 = new Employee2(1,"Susan", 56000);
		Employee2 emp8 = new Employee2(1,"Jhonny", 57000);
		Employee2 emp9 = new Employee2(1,"Tayrn", 58000);
		Employee2 emp10 = new Employee2(1,"Nick", 59000);
		
		
		/////Second Employee Class--should be John
		EmployeeUtil test = new EmployeeUtil();
		System.out.println((test.highestSalEmp(emp,emp2)).toString());
		
		//highest of the 10--should be nick
		System.out.println((test.highestTenSalEmp(emp,emp2,emp3,emp4,emp5,emp6,emp7,emp8,emp9,emp10)).toString());
		
		
		
		//increase 10% for first -- should be 55K
		test.increaseTenPercentSal(emp);
		System.out.println(emp.toString());
		
		
	}
	
}
