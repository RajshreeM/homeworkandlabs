
public class EmployeeUtil extends Employee2{
	
	
	
	public Employee2 highestSalEmp(Employee2 a, Employee2 b) {
		if(a.salary>b.salary) {
			return a;
		}
		return b;
	}
	
	public Employee2 highestTenSalEmp(Employee2 a, Employee2 b, Employee2 c, Employee2 d, Employee2 e, Employee2 f, Employee2 g, Employee2 h, Employee2 i, Employee2 j) {
		double max=0;
		Employee2 maxed = new Employee2();
		Employee2[] arr = {a,b,c,d,e,f,g,h,i,j};
		
		for(int p =0; p < arr.length; p++) {
			if(arr[p].salary > max) {
				maxed = arr[p];
				max= arr[p].salary;
			}
		}
		return maxed;
		
		
	}
	
	
	public void increaseTenPercentSal(Employee2 a) {
		double sal = a.salary;
		sal = (sal*.1) + sal;
		
		a.setSalary(sal);
	}
	
	//returns new emp obj with same info
	public Employee2 sameReturnEmployee(Employee2 a) {
		Employee2 b = new Employee2(a);
		return b;
	}
	
	
	//older Employee
	public Employee2 olderEmp(Employee2 a, Employee2 b) {
		int max = Math.max(a.age, b.age);
		if(a.age == max) {return a;}
		return b;
	}
	
	//updateSalary on condition
	public void updateSalOnCond(Employee2 a) {
		if(a.salary < 10000 && age > 35) {
			
			double sal = a.salary + (a.salary*.15);
			a.setSalary(sal);
		}
		
		else if(a.salary < 15000 && age > 45) {
			double sal = a.salary + (a.salary*.2);
					a.setSalary(sal);
		}
		else if(a.salary < 20000 && age > 55) {
			
			double sal = a.salary + (a.salary*.25);
			a.setSalary(sal);
		}
	}
	
	
	//calculates DA and HRA on cond
	public double getGrossSalary(Employee2 a) {
		double da;
		double hra;
		if(a.salary <= 10000) {
			da=.08*a.salary;
			hra=.15*a.salary;
		}
		else if(a.salary<=20000 && a.salary>10000) {
			da=.1*a.salary;
			hra=.2*a.salary;
		}
		else if(a.salary<=30000 && a.salary>20000 && a.age>=40) {
			da=.15*a.salary;
			hra=.27*a.salary;
		}
		else if(a.salary<=30000 && a.salary>20000 && a.age<40) {
			da=.13*a.salary;
			hra=.25*a.salary;
		}
		else {
			da=.17*a.salary;
			hra=.3*a.salary;
			
		}
		
		return da+hra+a.salary;
	}
}
