
public class Employee2 {
	
	int number;
	String name;
	double salary=10000;
	final String COMPANY_NAME ="XYZ_Comp";
	int age;
	
	
	
	public Employee2() {
		this.name="John";
		this.number=0;
	}
	
	public Employee2(Employee2 a) {
		this.name = a.name;
		this.number = a.number;
		this.salary=a.salary;
		this.age = a.age;
		
	}
	public Employee2(int number, String name, double salary) {
		this.number=number;
		this.name=name;
		this.salary=salary;
		
		
	}
	
	
	//displays Employee info
	@Override
	public String toString() {
		return "Name: "+this.name+ " Number: "+ this.number+ " Salary: "+this.salary;
	}
	
	public double HRA() {
		return salary*.2;
		
	}
	
	public void setSalary(double sal) {
		this.salary=sal;
	}
	
	public String compName() {
		return COMPANY_NAME;
	}
	
	//compares another employee to this one
	public Employee2 highestSal(Employee2 a) {
		if(this.salary>a.salary) {
			return this;
		}
		return a;
	}

}
