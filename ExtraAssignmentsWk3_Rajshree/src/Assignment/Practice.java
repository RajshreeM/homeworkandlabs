package Assignment;

import java.util.Scanner;
import java.util.Arrays; 

public class Practice {
	
	
	/*
	 * Used to test the accuracy of the methods written below
	 */
	public static void main(String[] args) {
		
		
		//Test should print out true, false true
		System.out.println("Test for SleepIn method: -- (true false true)");
		System.out.println(sleepIn(false, false));
		System.out.println(sleepIn(true,false));
		System.out.println(sleepIn(false,true));
		
		
		//Test for missing char
		System.out.println("Test for missingChar: ");
		System.out.println(missingChar("kitten",1));
		System.out.println(missingChar("kitten",0));
		System.out.println(missingChar("kitten",4));
		

		//Test for backAround
		System.out.println("\nTest for back around: ");
		System.out.println(backAround("cat"));
		System.out.println(backAround("Hello"));
		System.out.println(backAround("a"));
		
		//Test for name
		System.out.println("\nTest for hello + name: ");
		System.out.println( helloName("Bob"));
		System.out.println( helloName("Alice"));
		System.out.println( helloName("X"));
		
		
		//missing operation
		
		
		//pattern display
		displayPattern();
		
		//area and perimeter of circle test
		perimeterCircle(7.5);
		

		//testing compare numbers
		//compareNumbers();
		
		
		//thirdsum
		//thirdSum();
		
		//lowercase
		System.out.println(lowerCase("THE QUICK BROWN FOX JUMPS OVER THE LAZY DOG"));
		
		
		//check first last number in a set of 2 arrays
		Integer[] a = {50,-20,0,30,40,60,12};
		Integer[] b = {45,20,10,20,30,50,11};
		
		System.out.println(checkFirstLastArray(a,b));
		
		
		//combine first last should be [50,10]
		Integer[] c = {50,-20,0};
		Integer[] d = {5,-50,10};
		Integer[] result = combineFirstLast(c,d);
		System.out.println(result[0] + " " + result[1]);
		
		
		//contains 4 or 7 test
		Integer[] test = {5,7};
		System.out.println(contains4Or7(test));
		
		
		
		//starts with hello--not case sensitive
		
		System.out.println(startsWithHello("hElLothisisalongsentencewithnospaces"));
				
		//testing BMI
		//bodyMassIndex();
		
		
		//calculate
		//calculate();
		
		//index of an array elem
		Integer[] ind = {5,25,3,12,66,9,8,0};
		System.out.println("Index of 3 in array is in index: " + indexArray(ind, 3));
		
		//insert elem into array
		Integer[] elm = {5,25,3,12,66,9,8,0};
		System.out.println("Before inserting: " + Arrays.toString(elm) +"\nAfter inserting 55 at position 2: " + Arrays.toString(insertElem(elm,2,55)));
		
		//concatenate 2 strings
		System.out.println(concatString("PHP Excercises and ", "Python Excercises"));
		
		//Are the two strings equal?
		System.out.println("\"Stephen Edwin King\" equals \"Waler WInchell\"? " + equalStrings("Stephen Edwin King","Waler Winchell"));
		
		//hashcode
		System.out.println("THe hash for Python Exercises. is " + uniqueIdentifier("Python Exercises."));
		
		//touppercase
		System.out.println("Original String: The Quick BroWn FoX! \nWith uppercase: " +upperCase("The Quick BroWn FoX!"));
		
	}
	

	
	/*
	 * 
	 */
	public static boolean sleepIn(boolean weekday, boolean vacation) {
		return !weekday|| vacation;
	}
	
	/*
	 * 
	 */
	public static String missingChar(String str, int n) {
		String replaced = "";
		
		for(int i=0; i< str.length(); i++) {
			if(i!=n) {
			replaced += str.charAt(i);
			}
		}
		return replaced;
	}
	
	/*
	 * 
	 */
	public static String backAround(String str) {
		String replace = str.substring(str.length()-1);
		
		return replace+str+replace;
	}
	
	/*
	 * Hello + name
	 */
	public static String helloName(String name) {
		return "Hello "+name+ "!";
	}
	
	/*
	 * print specific operations
	 */
	public static void operation() {
		System.out.println(-5 + 8 * 6);
		System.out.println((55+9) % 9);
		System.out.println(20 + -3*5 / 8);
		System.out.println(5 + 15 / 3 * 2 - 8 % 3);
	}
	
	
	/*
	 * Displays a simple pattern
	 */
	public static void displayPattern() {
		System.out.println("  J   a  v   v  a");
		System.out.println("  J   a a  v  v  a a");
		System.out.println("J  J  aaaaa  V V  aaaaa");
		System.out.println(" JJ a   a    V   a  a");
	}
	
	public static void perimeterCircle(double radius) {
		double perm = 2 * Math.PI * radius;
		double area = radius*radius*Math.PI;
		
		System.out.println("Perimeter is = " + perm +"\nArea is = "+ area);
	}
	
	public static void compareNumbers() {
		Scanner sc = new Scanner(System.in);
		
		System.out.print("Input first integer: ");
		
		int first = sc.nextInt();
		
		System.out.print("Input second integer: ");
		int second = sc.nextInt();
	
		sc.close();
		
		
		
		//comparison
		if(first == second) {
			System.out.println(first + "=" + second);
		}
		else {
			System.out.println(first + "!=" + second);
		}
		if(first<second) {
			System.out.println(first + "<" + second);
			System.out.println(first + "<=" + second);
			
		}
		else {
			System.out.println(first + ">" + second);
		}
		
		
	}
	
	public static void thirdSum() {
		Scanner sc = new Scanner(System.in);
		
		System.out.print("Input first number: ");
		
		int a = sc.nextInt();
		
		System.out.print("Input second number: ");
		int b = sc.nextInt();
		
		
		System.out.print("Input third number: ");
		int c = sc.nextInt();
		sc.close();
		
		System.out.println("The result is " + (a+b==c));
		
		
		
	}
	
	public static String lowerCase(String str) {
		return str.toLowerCase();
	}
	
	public static boolean checkFirstLastArray(Integer[] a, Integer[] b) {
		
		return (a[0]==b[0] && a[a.length-1]==b[b.length-1]);
		
	}
	
	public static Integer[] combineFirstLast(Integer[] a, Integer[] b) {
		Integer[] c = new Integer[2];
		
		c[0] = a[0];
		c[1]=b[b.length-1];
		return c;
	}
	
	public static boolean contains4Or7(Integer[] a) {
		return a[0]==4|| a[0]==7 || a[1]==4 || a[1]==7;
	}
	
	public static boolean startsWithHello(String str) {
		return str.substring(0,5).equalsIgnoreCase("Hello");
	}
	
	
	public static void bodyMassIndex() {
		Scanner sc = new Scanner(System.in);
		
		System.out.print("Input weight in pounds: ");
		
		int a = sc.nextInt();
		
		System.out.print("Input height in inches: ");
		int b = sc.nextInt();
		
		System.out.print("Your BMI is:" + (double)703 * a / (double)(b*b));
		sc.close();
		
	}
	
	public static void calculate() {
		Scanner sc = new Scanner(System.in);
		
		System.out.print("Input first number: ");
		
		int f = sc.nextInt();
		
		System.out.print("Input second number: ");
		int s = sc.nextInt();
		sc.close();
		
		System.out.println("Sum of two integers: " + (f+s));
		System.out.println("Difference of two integers: " + (f-s));
		System.out.println("Product of two integers: " + f*s);
		System.out.println("Average of two integers: " + ((double)(f+s)/(double)2));
		System.out.println("Distance of two integers: " + (f-s));
		System.out.println("Max of two integers: " + Math.max(f, s));
		System.out.println("Min of two integers: " + Math.min(f, s));
		
		
		
	}
	
	
	public static int indexArray(Integer[] arr, int num) {
		
		for(int i=0; i <arr.length;i++) {
			if(arr[i] == num) {
				return i;
			}
		}
		//not found
		return -1;
	}
	
	/*18 &21
	 * Insert array elem to specific position in an array
	 */
	public static Integer[] insertElem(Integer[] a, int position, int num) {
		Integer[] b = new Integer[a.length +1];
		
		
		//up until the insert
		for(int i = a.length; i> position; i--) {
			b[i] = a[i-1];
		}
		//insert num
		b[position] = num;
		
		//rest of the array
		for(int i = position; i> 0; i--) {
			b[i-1] = a[i-1];
		}
		
		
		return b;
	}
	
	
	
	
	/*
	 * concat two strings
	 */
	
	public static String concatString(String a, String b) {
		return a+b;
	}
	
	/*
	 * equal strings?
	 */
	public static boolean equalStrings(String a, String b) {
		return a.equalsIgnoreCase(b);
	}
	
	/*
	 * Unique identifier
	 */
	public static int uniqueIdentifier(String str) {
		return str.hashCode();
	}
	
	public static String upperCase(String str) {
		return str.toUpperCase();
	}
}


