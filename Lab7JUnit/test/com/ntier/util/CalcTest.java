package com.ntier.util;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class CalcTest {
	private Calculator calc;
	
	@Before
	public void doSetUp() {
		calc = new Calculator();
	}

	@Test
	public void testAdd() {
		Calculator c = new Calculator();
		assertEquals(5.5,c.add(2.5,3),.001);
		fail("Not yet implemented");
	}

	@Test
	public void testSubtract() {
		Calculator c = new Calculator();
		assertEquals(-.5,c.subtract(2.5,3),.001);
	}

	@Test
	public void testMultiply() {
		Calculator c = new Calculator();
		assertEquals(7.5,c.multiply(2.5,3),.001);
	}

	@Test
	public void testDivide() {
		Calculator c = new Calculator();
		assertEquals(0.83,c.divide(2.5,3),.001);
	}

	@Test
	public void testStringLength() {
		
	}

	@Test
	public void testNumCompare() {
		
	}

	@Test
	public void testSumNumbers() {
		
	}

	@Test
	public void testCountValues() {
		
	}

	
	@After
	public void doTearDown() {
		calc = null;
	}
}
