package com.ntier.util;

public class Calculator {
	
	public double add(double num, double num2) {
		
	return num+num2;	
	}
	
	public double subtract(double num, double num2) {
		
		return num-num2;	
	}
	public double multiply(double num, double num2) {
		
		return num*num2;	
	}
	
	
	/*
	 * Integer division where num is the dividend and num 2 is the divisor
	 */
	public double divide(double num, double num2) {
		
		
		return num/num2;	
	}

	/*
	 * Returns the length of a given input string
	 */
	public int stringLength(String str) {
		return str.length();
	}
	
	/*
	 * Number comparator that tells if one first number is bigger than the other
	 */
	public boolean numCompare(int a, int b) {
		return a>b;
	}
	
	/*
	 * takes an array of numbers and returns the sum
	 */
	public int sumNumbers(Integer[] arr) {
		
		int result = 0;
		for(int i =0; i < arr.length;i++) {
			result += arr[i];
		}
		return result;
	}
	
	/*
	 * takes an array of integers and a search value and returns the number
	 * of occurences of that search value
	 */
	public int countValues(Integer[] arr, int val) {
		int found =0;
		
		for(int i=0;i<arr.length;i++) {
			if(arr[i]==val) {
				found++;
			}
		}
		return found;
	}
}
