package com.ntier.util;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class JUnitLifecycleTest {

	@Before
	public void doSetUpOne() {
		System.out.println("Setup 1");
	}
	@Test
	public void testOne() {
		System.out.println("Test 1");
		
	}
	@After
	public void doTearDownOne() {
		System.out.println("Teardown 1");

	}
	

	@Before
	public void doSetUpTwo() {
		System.out.println("Setup 2");
		
	}
	@Test
	public void testTwo() {
		System.out.println("Test 2");
	}
	@After
	public void doTearDownTwo() {
		System.out.println("Teardown 2");
		
	}


	@Before
	public void doSetUpThree() {
		System.out.println("Setup 3");
		
	}
	@Test
	public void testThree() {
		System.out.println("Test 3");
		
	}
	@After
	public void doTearDownThree() {
		System.out.println("Teardown 3");

		
	}
}
